package com.example.rent.sda_018_ap_person_listview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Switch sortSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sortSwitch = (Switch) findViewById(R.id.sortSwitch);
        ListView personListView = (ListView) findViewById(R.id.personList);
        PersonProvider personProvider = new PersonProvider();
        final List<Person> personList = personProvider.provide();
        final PersonAdapter personAdapter = new PersonAdapter(personList);


//        sortSwitch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Collections.sort(personList, new Comparator<Person>() {
//                    @Override
//                    public int compare(Person p1, Person p2) {
//                        return p1.getName().compareTo(p2.getName());
//                        // return p2.getAge() - p1.getAge();
//                    }
//                });
//                personAdapter.notifyDataSetChanged();
//            }
//        });
//
//        personListView.setAdapter(personAdapter);
//    }

                sortSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        Collections.sort(personList, new Comparator<Person>() {
                            @Override
                            public int compare(Person p1, Person p2) {
                                return p1.getName().compareTo(p2.getName());
                                // return p2.getAge() - p1.getAge();
                            }
                        });

                             } else {
                            Collections.reverse(personList);
                        }

                personAdapter.notifyDataSetChanged();
            }
        });

        personListView.setAdapter(personAdapter);
    }


    class PersonAdapter extends BaseAdapter {
        private final List<Person> personList;

        public PersonAdapter(List<Person> personList) {
            this.personList = personList;
        }

        @Override
        public int getCount() {
            return personList.size();
        }

        @Override
        public Person getItem(int position) {
            return personList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            TextView textView;
            if (convertView == null) {
                textView = new TextView(MainActivity.this);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
            } else {
                textView = (TextView) convertView;
            }
            Person person = getItem(position);
            textView.setText(person.getName() + ' ' + person.getAge());
            return textView;
        }
    }
}