package com.example.rent.sda_018_ap_person_listview;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by RENT on 2017-04-29.
 */

public class PersonProvider {

    public List<Person> provide() {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Jan", 20));
        persons.add(new Person("Halina", 25));
        persons.add(new Person("Adam", 9));
        return persons;
    }
}